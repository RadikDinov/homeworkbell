import {ADD_TODO, TOGGLE_TODO} from './actions';

let initialState = [
  {
    title: 'TODO example',
    completed: true
  }
]

export default function todoReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          title: action.payload,
          completed: false
        }
      ]
    case TOGGLE_TODO:
      return state.map((todo, index) => {
        if (index === action.payload) {
          return { ...todo, completed: !todo.completed }
        }
        return todo
      })
    default:
      return state
  }
}