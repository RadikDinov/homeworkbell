import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import TodoListApp from './TodoListApp';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {store} from './store'

ReactDOM.render(<Provider store={store} >
                  <TodoListApp />
                </Provider>, document.getElementById('root'));
registerServiceWorker();
