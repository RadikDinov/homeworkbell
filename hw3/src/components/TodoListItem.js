import React, { Component } from 'react';

class TodoListItem extends Component {

  render() {
    return (
      <li className="TodoListItem" onClick={this.props.toggleTodo}>
        <label>{this.props.title}</label>
        {this.props.completed ? <i>(Сделано!)</i> : ''}
      </li>
    );
  }
}

export default TodoListItem;
