import React, { Component } from 'react';

class TextAdder extends Component {

  handleClick = () => {
    this.props.addText(this.refs.input.value);
  }

  render() {
    return (
      <div className="TextAdder">
          <input type="text" ref="input" />
          <button onClick={this.handleClick} >Добавить</button>
      </div>
    );
  }
}

export default TextAdder;
