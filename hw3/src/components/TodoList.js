import React, { Component } from 'react';
import TodoListItem from './TodoListItem';

class TodoList extends Component {

  render() {
    //Создаем список JSX компонентов
    let todoItems = this.props.todoItems.map(
      (todoItem, index) => {
        return (
          <TodoListItem toggleTodo={this.props.toggleTodo.bind(null, index)} 
                        key={index} 
                        title={todoItem.title} 
                        completed={todoItem.completed} />
        )
      }
    );
    return (
      <ul className="TodoList">

        {todoItems}

      </ul>
    );
  }
}

export default TodoList;
