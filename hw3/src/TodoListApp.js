import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux';
import {ADD_TODO, TOGGLE_TODO} from './actions';
import TextAdder from './components/TextAdder';
import TodoList from './components/TodoList';


class TodoListApp extends Component {

  addTodo = (text) => {
    this.props.addTodo(text);
  }

  toggleTodo = (index) => {
    this.props.toggleTodo(index);
  }

  render() {
    return (
      <div className="TodoListApp">

        <TextAdder addText={this.addTodo.bind(this)} />

        <TodoList todoItems={this.props.store} toggleTodo={this.toggleTodo} />

      </div>
    );
  }
}

export default connect(
  store => ({
    store: store
  }),
  dispatch => ({
    addTodo: (text) => dispatch({ type: ADD_TODO, payload: text }),
    toggleTodo: (index) => dispatch({ type: TOGGLE_TODO, payload: index })
  })
)(TodoListApp)
