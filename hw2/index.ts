      interface Showable {
        showLabel(): string;
        showInfo(): string;
      }

      class Vehicle implements Showable {
        public name: string;
        public description: string;

        constructor(name: string, description: string){
          this.name = name;
          this.description = description;
        }

        showLabel(): string {
          return this.name;
        }

        showInfo(): string {
          return  `Модель: ${this.name} <br>
                  Примечание: ${this.description}`;
        }
      }

      class Truck extends Vehicle{
        public carrying: number;

        constructor(name: string, description: string, carrying: number){
          super(name, description);
          this.carrying = carrying;
        }

        showLabel(): string {
          return `${this.name} ${this.carrying}`;
        }

        showInfo(): string {
          return  `Тип: Грузовая <br>
                  Модель: ${this.name} <br>
                  Вес груза: ${this.carrying} тонн <br>
                  Примечание: ${this.description}`;
        }
      }

      class Car extends Vehicle{
        public velocity: number;

        constructor(name: string, description: string, velocity: number){
          super(name, description);
          this.velocity = velocity;
        }

        showLabel(): string {
          return `${this.name} ${this.velocity}`;
        }

        showInfo(): string {
          return  `Тип: Легковая <br>
                  Модель: ${this.name} <br>
                  Скорость: ${this.velocity} км/ч <br>
                  Примечание: ${this.description}`;
        }
      }

      interface VehicleClickWrapper<T>{
        vehicle: T;
        onClick();
      }

      let vehicles: VehicleClickWrapper<Vehicle>[] = [];
      let vehicleForm: HTMLFormElement = document.forms['vehicle_form'];
      vehicleForm.onsubmit = function addVehicle(): boolean{
        //Получаем объект транспорта
        let addedVehicle: Vehicle = (function getVehicle(): Vehicle {
          let name: string = this.elements["vehicle_model"].value;
          let description: string = this.elements["vehicle_description"].value;

          if(this.elements["vehicle_type"][0].checked) {
            let carrying: number = Number(this.elements["carrying_or_velocity"].value);
            return new Truck(name, description, carrying);
          } else if (this.elements["vehicle_type"][1].checked) {
            let velocity: number = Number(this.elements["carrying_or_velocity"].value);
            return new Car(name, description, velocity);
          }

          return new Vehicle(name, description);
        }).call(this);

        //Добавляем объект транспорта в массив
        vehicles.push(
          {
          vehicle: addedVehicle,
          onClick: addedVehicle.showInfo.bind(addedVehicle)
          }
        );

        //Обновляем список по случаю изменения содержания массива
        (function refreshVehiclesList(): void {
          let listOfVehiclesContainer: HTMLElement = document.getElementById("list_of_vehicles");
          let vehicleListElement: any;
          let vehicleDescription: HTMLElement = document.getElementById("vehicle_description");
          listOfVehiclesContainer.innerHTML = '';
          for(let i: number = 0; i < vehicles.length; i++) {
            vehicleListElement = document.createElement("li");
            vehicleListElement.innerHTML = vehicles[i].vehicle.showLabel();
            vehicleListElement.vehicles = vehicles;
            vehicleListElement.index = i;
            vehicleListElement.onclick = function showVehicleInfo() {
              //Меняем строку описания транспорта при клике на элемент списка
              vehicleDescription.innerHTML = vehicles[this.index].onClick();
            }
            listOfVehiclesContainer.appendChild(vehicleListElement);
          }
        })();
        
        //Отменяем отправку запроса на сервер
        return false;
      }

      //Меняем label "Масса/Скорость" при нажатии на переключатель
      function changeLabel(label: HTMLElement, value: string): void {
        label.innerHTML = value;
      }

      let vehicleTypeTruck: HTMLElement = vehicleForm.elements["vehicle_type"][0];
      let vehicleTypeCar: HTMLElement = vehicleForm.elements["vehicle_type"][1];
      let label: HTMLElement = document.getElementById('carrying_or_velocity_label');
      vehicleTypeTruck.onclick = changeLabel.bind(this, label, "Вес груза (тонн): ");
      vehicleTypeCar.onclick = changeLabel.bind(this, label, "Скорость (км/ч): ");
