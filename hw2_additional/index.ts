class TaskList<Task extends ITask> {
  taskList: Array<Task> = [];

  add(Task){
    this.taskList.push(Task);
  }

  clear(){
    this.taskList = [];
  }

  startAll(){
    this.taskList.forEach(element => {
      element.start();
    });
  }
}

interface ITask{
  title: string;
  start();
}

class Task implements ITask{
  title: string;

  constructor(title:string){
    this.title = title;
  }
  
  start(){
    console.log(`task ${this.title} is started`);
  }
}

class SimpleTask extends Task{
  content: string;

  constructor(title: string, content: string){
    super(title);
    this.content = content;
  }
  
  start(){
    console.log(`task ${this.title} with content ${this.content} is started`);
  }
}



let taskList = new TaskList<Task>();
taskList.add(new SimpleTask("simple1", "simple_content1"));
taskList.add(new SimpleTask("simple2", "simple_content2"));
taskList.add(new Task("task1"));
taskList.add(new Task("task2"));
taskList.startAll();